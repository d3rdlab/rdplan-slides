## Innovation Lab
### Roadmap 2018-2019 Planning
---

## Possible R&D Scenarios

- Next generation overlays
- Monetization
- Content Discovery
- Social Features

+++

## Next generation overlays (1)

- 3D data viz / augmented reality in video
- 2D/3D stats overlay on live tracked objects/players
    - trajectories, names, distance, tactics, heatmaps, player positions, ball, etc.
- Interactive TV-like overlays
    - cloud or client-side rendering
    - (something OnStage done/is exploring?)

+++

## Next generation overlays (2)

- Push-mode based on user profile, rules, settings, sport type, game events, timing
    - a sort of automatic graphics director
- Commentary (on-demand/live) transcription / automatic translation
- Subtitles / artificial voice for impaired users
- Vocal Assitants Support (Google Home / Amazon Echo)

+++

## Monetization (1)

Within the DIVA platform, offer a way for branding, advertising, monetize/sell third-party/partners products like jerseys, tickets or highlight banners, brands, etc. 
- Immersive ads / augmented reality ads
- Contextual Advertisements/Product placement in video feed (i.e. AR objects placed in the game area, around a player)
- Within the video experience, offer products like jerseys, tickets, or partners products to a captive audience in a non-intrusive way
- Offer relevant products regarding the actions (scorer's jersey, tickets, partners products...)

+++

## Monetization (2)

![Concept](/assets/images/MonetizationRD.png)
@css[img-caption](*Concept*)

+++

## Content Discovery (1)

- Automatic video indexing 
    - on new & old video clips, useful to enrich tagged and untagged videos
- Automated video highlights
    - pick up "key moments" in the match by deriving data from players, fans, stats, audio from feed

+++

## Content Discovery (2)

- Search & recommendation engine
    - Video content (actions, players, sport events, scoring, etc.)
    - User profile, friend suggestions, trends, etc.
    - Binge watching, watch history, favorites, pause/resume across devices
    - Per-user customization
- Automatic narrative @fa[arrow-right] data to stories
    - automatic generated for editorial

+++

## Social Features

- (Semi/Full) Automatic video clips generation for sharing on social networks (with graphics/audio customization)

---

@title[Activities]

To enabling one or more of these possible scenarios, a number of **common and preparatory R&D activies** has been identified.

Depending on the selected activities, research boundaries and final results, each scenario may be enabled fully, partially or not at all.

+++

### Face recognition & matching

Find where/who a face is in a picture and/or video

- Est. feasibility: @css[feasibility-green](95%)
- There are already some 3rd party services to experiment with and verify offline/real-time/video capability.
- High precision, video and/or real-time may require custom AI models (est. feasibility: @css[feasibility-red](50%))

+++

### Face recognition & matching (2)

- Enabling scenarios:
    - **Next-gen overlays**: 2D/3D stats overlayed on live tracked players
    - **Monetization**: contextual ads, product placement, brand/player identification
    - **Content discovery**: automatic video indexing/tagging, automatic highlights, search&recommendation

+++

### Brands/logo/object recognition & matching

Find where/which logo/brands is in a picture and/or video. 

- Est. feasibility: @css[feasibility-green](95%)
- There are already some 3rd party services to experiment with and verify offline/real-time/video capability.
- High precision, video and/or real-time may require custom AI models (est. feasibility: @css[feasibility-red](50%))

+++

### Brands/logo/object recognition & matching (2)

- Enabling scenarios:
    - **Monetization**: contextual ads, product placement, brand/player identification
    - **Content discovery**: automatic video indexing/tagging, automatic highlights, search&recommendation

+++

### Video Shot Change Detection

Detect scene changes within a video

- Est. feasibility: @css[feasibility-green](95%)
- Pre-requirement for automatic video generation / custom AI video analytics
- There are already some 3rd party services to experiment with and verify real-time capability.
- If real-time is not supported, custom logic is required (est. feasibility: @css[feasibility-yellow](75%))

+++ 

### Video Shot Change Detection (2)

Detect scene changes within a video

- Enabling scenarios:
    - **Next-gen overlays**: automatic graphics/overlays director
    - **Content discovery**: automatic video indexing/tagging, automatic highlights, search&recommendation
    - **Social features**: automatic clipping for video production
+++

### DIVA Player Customization

Implement/develop custom plugin or overlays to support new features

- Est. feasibility: @css[feasibility-green](95%)
- Requirement for other R&D activities (Next-gen overlays, Monetization, Content Discovery, Social Features)
- Help from DIVA dev team may needed

+++ 

### Video Workflow Customization

Implement/develop custom video workflow to support video overlays, clip generation, etc.

- Est. feasibility: @css[feasibility-green](90%)
- Requirement for other R&D activities (Next-gen overlays, Content Discovery, Social Features)
- Help from DIVA (or other) dev teams may needed

+++

### Automatic Video Analysis

Extract insights, annotations, text OCR, audio transcriptions and other useful metadata from video.

- Est. feasibility: @css[feasibility-yellow](75%)
- There are already some 3rd party services to experiment with (**NO real-time capability**)
- High precision or real-time may require custom AI models (est. feasibility: @css[feasibility-red](50%))

+++

### Automatic Video Analysis (2)

- Enabling scenarios:
    - **Next-gen overlays**: automatic graphics/overlays director; commentary transcription/translation
    - **Monetization**: contextual ads, brand/player/actions identification 
    - **Content discovery**: automatic video indexing/tagging, automatic highlights, search&recommendation

+++

### Person/object tracking, recognition & matching

Identify and follow people and/or objects within a video.

- Est. feasibility: @css[feasibility-yellow](75%)
- There are no known 3rd party product/services. All custom AI models/tools.
- Enabling scenarios:
    - **Next-gen overlays**: 2d/3d stats overlays on tracked players
    - **Monetization**: contextual ads, brand/player/actions identification 
    - **Content discovery**: automatic video highlights, search&recommendation

+++

### Generate artificial voice from text/data

For impaired users or other customization/voice-over

- Est. feasibility: @css[feasibility-yellow](75%)
- There are already some 3rd party services to experiment with and verify real-time support
- Enabling scenarios:
    - **Next-gen overlays**: automatic subtitles / artificial voice

+++ 

### Recommendation engine

Given user profiles and video contents+metadata, match them for compatibility.

- Est. feasibility: @css[feasibility-yellow](75%)
- Research papers only, no known projects/tools for recommendation engine creation.
- Enabling scenarios:
    - **Content discovery**: search&recommendation

+++

### Data Analysis, end-user behavior and pattern recognition

Access/dump/extract/combine data (possibly a lot) on users, events, sport games, statistics, tracking

- Pre-requirement for per-user customization, automatic narrative, recommendation engine.
- Research papers only, no projects/tools for behavior understanding/recognition.
- Custom AI model needed, with lots of data for training

+++

### Data Analysis, end-user behavior and pattern recognition (2)

- Est. feasibility: @css[feasibility-yellow](65%)
- Enabling scenarios:
    - **Next-gen overlays**: automatic graphics/overlays director; per-user customization
    - **Monetization**: contextual ads; per-user customization
    - **Content discovery**: automatic video indexing/tagging, automatic highlights, search&recommendation
    - **Social features**: per-user videoclip highlights for sharing

+++

### 3D Engine Integration / WebGL

Explore Unity3D + Exodus or alternative engines (HTML/WebGL) to implement cloud/client-side graphics.

- Est. feasibility: @css[feasibility-yellow](65%)
- Something already in the work by OnStage team?
- Enabling scenarios:
    - **Next-gen overlays**: 3D data / AR graphics
    - **Next-gen overlays**: 2D/3D overlays cloud/client-side
    - **Next-gen overlays**: interactive TV-like overlays

+++ 

### Automatic narrative 

Extract insights, sentiment, language, other useful metadata from statistics data, commentary to generate editorial content

- Est. feasibility: @css[feasibility-red](50%)
- There are already some 3rd party services to experiment with text analysis.
- Research papers only, no known projects/tools for automatic text generation.
- Enabling scenarios:
    - **Next-gen overlays**: automatic subtitles / artificial voice
    - **Content discovery**: data to stories (editorial); search&recommendation

+++ 

### Automatic camera pose/calibration detection from picture/video feeds

Pre-requirement for 3D overlay placement in pictures/videos; custom AI video analytics

- Est. feasibility: @css[feasibility-red](50%)
- Research papers only, no known projects/tools to use.
- Enabling scenarios:
    - **Next-gen overlays**: 2d/3d stats overlays on tracked players
    - **Next-gen overlays**: 3D data / AR graphics
    - **Monetization**: immersive/AR ads; product placement

+++ 

### Full body skeleton tracking

Identify and follow people within a video, and for each estimate 3D body parts position (like Kinect, but on videos)

- Est. feasibility: @css[feasibility-red](50%)
- Research papers only, no known projects/tools to use.
- Enabling scenarios:
    - **Next-gen overlays**: 2d/3d stats overlays on tracked players
    - **Content discovery**: automatic video indexing/tagging, automatic highlights, search&recommendation

---
### Scenario-Activity dependencies

<div class="img-report">
![Scenario-Activity dependencies](/assets/images/report_2.png)
@css[img-caption](C = CAN; S = SHOULD; M = MUST)
</div>
---

### Activity estimates

![Activity estimates](/assets/images/report_3.png)

---

### Scenario-Activity estimates

<div class="img-report">
![Scenario-Activity dependencies](/assets/images/report.png)
</div>
@css[img-caption](*supposing to develop S and M activities only)

---

### Request for feedback

@css[text-par1](If one or more of the identified scenario/activities are of your interest, and you are available for further discussions and/or feedback, please fill this short <3mins survey here:)

https://it.surveymonkey.com/r/7NHLR5K

@css[text-par2](Top 2/3 activities will be further analyzed and PoCs put in roadmap)

---

### Thank you